import simplematrixbotlib as botlib
import json
import toml
from typing_extensions import TypedDict

class feedMeta(TypedDict):
    etag: str
    modified: str
    def __init__(self, etag: str, modified: str):
        self.etag = etag
        self.modified = modified
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True,ensure_ascii=False, indent=4)
def validate_auth(config):
    if not any(auth_method in config for auth_method in ['password', 'access_token']):
            config['password nor access_token']
        
def process_new_objects(new_objects, sent_objects):
    """
    Compares a list of new objects with the list of already sent objects based on the 'id' field.
    Returns the new objects that have not yet been processed.

    Args:
    - new_objects (list): A list of objects to be processed, each object should have an 'id' field.
    - sent_objects (list): A list of objects that have already been sent, each object should have an 'id' field.

    Returns:
    - list: A list of new objects that have not yet been sent.
    """
    sent_ids = {obj['id'] for obj in sent_objects}  # Convert sent objects to a set of IDs for O(1) lookups
    out_objects = [obj for obj in new_objects if obj['id'] not in sent_ids]
    
    print(f"len obj: {len(out_objects)}")
    return out_objects

def validate_config(file_name):
  config = toml.load(file_name)
  try:
    config['homeserver']
    config['username']
    config['interval']
    config['slo_time_to_run']
    config['bridge']
    config['prometheus_port']
    config['ff_tracemalloc']
    validate_auth(config)
    for bridge in config['bridge']:
      bridge['name']
      bridge['feed_url']
      bridge['room_id']
  except KeyError as e:
    raise ValueError(f"{e} not found in {file_name}")
  return config

def bot_factory(creds: botlib.Creds, func, args) -> botlib.Bot:
    bot = botlib.Bot(creds)

    @bot.listener.on_startup
    async def startup(room_id):
        await func(bot, *args)
    
    return bot