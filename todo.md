renovate bot is not updating the poetry.lock file like it should be
renovate bot should run under a project in the matrix-rss-bridge instead of my own hosted one
dev env - stand up a web server with rss feed to generate mock data
dev env - stand up a web server with atom feed to generate mock data
dev env - generate synapse user and room that we can use for testing
dev env - generate unique homeserver secrets
Document how users can adjust the message template through the config['message_template']
all config options should have sane defaults. currently the app will crash if a config key is missing from the toml file.