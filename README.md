# matrix-rss-bridge

A bridge for reading RSS feeds in Matrix rooms.

## Setup

Requires Python ^3.8

```sh
python3 -m pip install poetry
git clone https://gitlab.com/imbev/matrix-rss-bridge.git
cd matrix-rss-bridge
python3 -m poetry install
```

### Docker Compose

Run `docker compose -f "docker-compose.yaml" up -d --build`, or `make compose_up` and wait for all services to come up. This will build the bot as a docker image, and run synapse, element web, and a small monitoring stack. There is some manual work to provision users and start the bot. Browse to Element Web at http://localhost:8449/#/register 

use this for the server url `http://localhost:8448`. Enter your name, and generate a password

you also need to register an account for the bot. Open a private window or a new web browser and repeat the registration steps with the name `rss-bot`. For developmeent I use the same generated password for my user account and the bot account. 

From your user account, create a private room and enable end to end encryption. After creation, invite the `rss-bot`, and copy the room link either from the left hand menu, or from the address bar at the top of your browser. it will look like: `http://localhost:8449/#/room/!PJeDoBtUGpvFPWZmAL:192.168.0.1`. We need `!PJeDoBtUGpvFPWZmAL`  From the bot window, accept the invite. 


## Usage

### Config

Create `config.toml` to configure the bridge.

```toml
# config.toml
homeserver = "https://example.com"
username = "username" 
password = "password"
interval = 60 # seconds
slo_time_to_run = 120
feed_path = "/mnt/feeds"
prometheus_port = 8000
ff_tracemalloc = false

[[bridge]]
    name = "matrix.org blog"
    feed_url = "https://matrix.org/blog/feed"
    room_id = "!AUweUQXCxcVfFOaOIU:matrix.org"
    # template_markdown = """\
    # <h1>{{title}}</h1>\n\n{{published}}\n{{summary}}\
    # """
```
If you don't want to save your password, then you can use the bridge account's access token.

```toml
# config.toml
homeserver = "https://example.com"
username = "username"
access_token = "some_long_string_that_represents_my_access_token"
interval = 60 # seconds
slo_time_to_run = 120
feed_path = "/mnt/feeds"
prometheus_port = 8000
ff_tracemalloc = false

[[bridge]]
    name = "matrix.org blog"
    feed_url = "https://matrix.org/blog/feed"
    room_id = "!AUweUQXCxcVfFOaOIU:matrix.org"
    # template_markdown = """\
    # <h1>{{title}}</h1>\n\n{{published}}\n{{summary}}\
    # """
```

#### feed_path

There are 2 types of persisted data, some header info (etag and modified headers), and all the articles. This parameter controls where those files are saved. If this location is empty, on the first run you will get all the articles sent to your channel.

#### slo_time_to_run

Used by the prometheus monitoring to create alerts for if the bot has not checked on the rss sources often enough for me.

#### prometheus_port

What port to bind to in order to surface prometheus metrics via http endpoint

#### ff_tracemalloc

Used to enable `tracemalloc` memory profiling on the application loop. Should only be set to true during development.

### Running

```sh
python3 -m poetry run bridge
```

## Docker

- Build the docker image:

```
docker build . -t matrix-rss-bridge
```

- Create `config.toml` from [here](#config)
- Run bridge

```
docker run -v $(pwd)/config.toml:/app/config.toml:ro matrix-rss-bridge:latest
```

- **Optionally** it can be build and run with `docker-compose` with the [`docker-compose.yaml`](docker-compose.yaml) file.

    - build the image: `docker-compose build`
    - run the service: `docker-compose up -d`
    - check logs: `docker-compose logs -f`
    - stop the service: `docker-compose stop`
    - bring down the service: `docker-compose down`

## Misc

- Free and Open Source, Licensed under the GPL-3.0-only license.
