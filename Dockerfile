FROM python:3.12.5@sha256:11aa4b620c15f855f66f02a7f3c1cd9cf843cc10f3edbcf158e5ebcd98d1f549 as python
WORKDIR /app

FROM python as poetry
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:$PATH"
COPY pyproject.toml poetry.lock ./
RUN pip install poetry && poetry install --no-interaction --no-ansi -vvv

FROM python:3.12.5-slim@sha256:c24c34b502635f1f7c4e99dc09a2cbd85d480b7dcfd077198c6b5af138906390 as runtime
RUN pip install pytest

ENV PATH="/app/.venv/bin:$PATH"
WORKDIR /app
COPY --from=poetry /app /app
COPY . .
RUN pytest

CMD [ "python", "/app/matrix_rss_bridge/main.py" ]
